package me.ddevil.shiroi.craft.misc

annotation class PluginSettings(
        val aliases: Array<String>
)
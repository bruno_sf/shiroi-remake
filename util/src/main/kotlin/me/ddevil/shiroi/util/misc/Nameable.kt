package me.ddevil.shiroi.util.misc

interface Nameable {

    var name: String

    var alias: String

}
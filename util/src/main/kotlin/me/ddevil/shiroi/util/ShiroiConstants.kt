package me.ddevil.shiroi.util

object ShiroiConstants {
    const val NAMEABLE_NAME_IDENTIFIER = "name"
    const val NAMEABLE_ALIAS_IDENTIFIER = "alias"
    const val X_IDENTIFIER = "x"
    const val Y_IDENTIFIER = "y"
    const val Z_IDENTIFIER = "z"
    const val ALPHA_IDENTIFIER = "a"
    const val PARENT_IDENTIFIER = "parent"
}
